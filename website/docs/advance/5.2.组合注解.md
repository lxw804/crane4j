## 组合注解

在 Spring 环境中，可以尝试使用 Spring 的合成注解优化 `crane4j` 的注解配置：

~~~java
// 将目标注解作为元注解
@Assemble(key = "id", container = "student", props = @Mapping(src = "studentName", ref = "name"))
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AssembleStudent { }
~~~

然后直接使用合成注解 `@AssembleStudent` 代替原本的一大坨：

~~~java
public class Student {
    // 直接使用合成注解
    @AssembleStudent
    private Integer id;
    private String name;
}
~~~

`crane4j` 几乎所有的注解都支持 spring 的合成注解，若有必要，在非 spring 环境也可以通过 `AnnotationFinder` 接口去实现类似的功能，让相关组件支持定义的注解。

:::warning

注意，不支持解析复数可重复的组合注解，即若 `A` 是可重复注解，则当在一个元素上添加多个 `A` 时，将无法正确解析到该注解。

:::