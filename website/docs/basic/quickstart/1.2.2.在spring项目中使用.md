## 安装

### 引入依赖

在 Spring 项目中，请引入 `crane4j-spring-extension` 依赖：

~~~xml
<dependency>
    <groupId>cn.crane4j</groupId>
    <artifactId>crane4j-extension-spring</artifactId>
    <version>${last-version}</version>
</dependency>
~~~

并请确保已经引入 `spring-context`、`aspectjweaver` 依赖。

> 当前最新版本为 ![maven-central](https://img.shields.io/github/v/release/Createsequence/crane4j?include_prereleases)

### 启用配置

`crane4j` 已经准备好了默认的配置类 `DefaultCrane4jSpringConfiguration`，用户仅需在自己的项目通过下述任意方式将其纳入 Spring 容器管理即可：

- 在 `xml` 或者配置类中手动创建 `DefaultCrane4jSpringConfiguration` 实例；
- 在任意配置类上通过 `@Import` 注解引入 `DefaultCrane4jSpringConfiguration.class`；

确保在项目启动后，上述配置类中配置的组件都已注册到 spring 上下文中。

## 配置数据源

在 `crane4j` 中，一个数据源对应一个**数据源容器**，因此在进行填充操作之前，需要先准备好相应的数据源容器。

下面是一个简单的示例，配置了一个命名空间为 `gender` 的数据源容器，根据 0 或 1 返回对应的性别名称：

```java
// 注册性别信息数据源
Map<Integer, String> sources = new HashMap<>();
sources.put(0, "女");
sources.put(1, "男");
Container<String> genderContainer = Containers.forMap("gender", sources);
```

接着，我们将其注册从 spring 容器获得的全局配置类 `Crane4jGlobalConfiguration` 中：

~~~java
Crane4jGlobalConfiguration context = SpringUtil.getBean(Crane4jGlobalConfiguration.class);
context.registerContainer(genderContainer);
~~~

:::tip

- 注册数据源的步骤我们也可以放到 `@PostConstruct` 注解方法或者其他声明周期回调中；
- 处简单的本地缓存外，`crane4j` 还支持创建其他种类的数据源容器，具体参见后文数据源容器部分内容；

:::

## 配置装配操作

在`crane4j`中，**装配操作**是指对某个对象的填充行为。我们可以通过在类或属性上添加注解来配置装配操作。

例如，在`Student`类中声明了一个装配操作：

- 该操作基于`sex`字段的值来执行，即根据`sex`字段的值查找关联数据。
- 从命名空间为`gender`的数据源容器获取关联数据。
- 将获取的关联数据直接赋值给`sexName`引用字段。

~~~java
@RequiredArgsConstructor // 使用 lombok 生成 get 方法和构造器
@Getter
@Setter
public class Student {
    private final String name;

    @Assemble(
        container = "gender", // 指定使用的命名空间为 gender 的数据源容器
        props = @Mapping(ref = "sexName") // 将根据 sex 取得值映射到 sexName 上
    )
    private final Integer sex;
    private String sexName;
}
~~~

以上配置表示根据`sex`字段从`gender`数据源容器中查找对应的男/女名称，并映射到`sexName`字段上。

:::tip

- `@Assemble` 注解用于声明一次填充操作，具体参见后文[装配操作](./../operation/3.1.声明装配操作.md)一节；
- `@Mapping` 用于指明数据源对象上的字段要如何映射到待处理对象的字段上，具体参见后文[字段映射](./../operation/3.4.配置字段映射.md)一节；

:::

## 执行装配操作

在项目启动后，我们可以从 spring 容器中获取填充工具类 `OperateTemplate` 去填充我们已经配置过的对象：

~~~java
OperateTemplate operateTemplate = SpringUtil.getBean(OperateTemplate.class);
List<Student> students = Arrays.asList(
    new Student("小红", 1), new Student("小明", 2)
);
operateTemplate.execute(students);
~~~

在执行后，`students` 中对象的 `sexName` 将根据 `sex` 字段的值被填充：

~~~java
[
    {
        "name": "小红",
        "sex": 0,
        "sexName": "女"
    },
    {
        "name": "小明",
        "sex": 1,
        "sexName": "男"
    }
]
~~~

## 执行嵌套填充

当我们需要填充嵌套对象时，除了为嵌套字段声明装配操作外，还需要为需要填充的嵌套字段声明**拆卸操作**。

例如，`Student`对象中嵌套了一个需要填充的`StudentClass`对象。该对象需要通过 id 从命名空间为 `student-class` 的数据源容器获取数据源对象的 `name` 属性，并将其填充到自己的 `name` 字段上。

除了为 `StudentClass` 类声明装配操作外，我们还需要在 `Student` 的 `studentClass` 属性上通过注解声明一个拆卸操作：

~~~java
@RequiredArgsConstructor
@Data
public class Student {
    private final String name;

    @Assemble(
        container = "gender", // 指定使用的命名空间为 gender 的数据源容器
        props = @Mapping(ref = "sexName") // 将根据 sex 取得值映射到 sexName 上
    )
    private final Integer sex;
    private String sexName;
    
    // 声明一个拆卸操作，拆卸后需要填充的对象类型为 StudentClass
    // 字段类型可以是单个对象，数组或者 Collection 集合
    @Disassemble(type = StudentClass.class)
    private final StudentClass studentClass; 
}

@RequiredArgsConstructor
@Data
public class StudentClass {
    @Assemble(container = "student-class", @Props = @Mapping("name"))
    private final Integer id;
    private String name;
}
~~~

以上配置表示，在执行装配操作之前，我们需要先将 `Student` 中的 `StudentClass` 取出并摊平，然后再完成所有待处理的 `Student` 和 `StudentClass` 对象的装配操作。

我们依然在项目启动后执行下述代码：

~~~java
OperateTemplate operateTemplate = SpringUtil.getBean(OperateTemplate.class);
List<Student> students = Arrays.asList(
    new Student("小红", "0", new StudentClass(1)), new Student("小明", "1", new StudentClass(2))
);
operateTemplate.execute(students);
~~~

执行后，`students` 中的 `Student` 对象及嵌套的 `StudentClass` 对象将会被填充：

~~~json
[
    {
        "name": "小红",
        "sex": 0,
        "sexName": "女"
        "studentClass": {
            "id": 1,
            "name": "一年1班"
        }
    },
    {
        "name": "小明",
        "sex": 1,
        "sexName": "男"
        "studentClass": {
            "id": 2,
            "name": "一年2班"
        }
    }
]
~~~
