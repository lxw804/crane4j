## 2.3.1. 基本使用

可以使用 `Containers.forConstantClass` 方法将常量类定义为一个数据源容器：

```java
@ContainerConstant
public static class FooConstant {
    public static final String ONE = "one";
    public static final String TWO = "two";
    public static final String THREE = "three";
}

// 使用 Containers.forConstantClass 方法构建容器
// 容器缓存的数据为： {"ONE" = "one"}, {"TWO" = "two"}, {"THREE" = "three"}
Container<String> container = Containers.forConstantClass(FooConstant.class, new SimpleAnnotationFinder());
```

或者，也可以通过建造者构建一个常量容器：

~~~java
Container<?> container = ConstantContainerBuilder.of(FooConstant.class)
    .namespace("test") // 指定容器命名空间
    .onlyPublic(false) // 扫描所有公有和非公有属性
    .reverse(true) // 翻转键值对，即使用属性名作为value，属性值作为key
    .build();
~~~

相比起通过静态工厂方法构建，建造者提供更多的配置项。

## 2.3.2. 可选注解

同样地，我们也支持通过注解配置容器的一些更具体的参数：

```java
@ContainerConstant(
    namespace = "foo", // 指定命名空间
    onlyExplicitlyIncluded = true,  // 是否只保存带有 @Include 注解的属性
    onlyPublic = false // 是否只保存公共变量
)
public static class FooConstant2 {
    @ContainerConstant.Include      // onlyExplicitlyIncluded 为 true 时，仅包含带有该注解的属性
    public static final String ONE = "one";
    @ContainerConstant.Exclude      // 默认情况下排除该属性
    public static final String TWO = "two";
    @ContainerConstant.Name("THREE") // 指定 key 名称为 "THREE"
    private static final String SAN = "three";
}
```

这里提供了一些可选的配置：

- `namespace`：构建容器的命名空间；
- `onlyExplicitlyIncluded`：是否只保存带有 `@Include` 注解的属性；
- `onlyPublic`：是否只保存公共变量；

此外，我们还引入了一些内部注解，与 `Lombok` 类似，可以配合使用以达到特定的效果：

- `@ContainerConstant.Include`：与 `onlyExplicitlyIncluded` 属性配合使用，声明要保留的常量属性；
- `@ContainerConstant.Name`：在将常量属性注册到容器后，使用注解指定的名称替代属性名；

**反转键值对**

除了基本配置之外，还有一个特殊的属性 `reverse`，将其设置为 `true` 后，可以反转常量属性名和属性值的关系。例如：

```java
@ContainerConstant(reverse = true)
public static class FooConstant {
    public static final String ONE = "one";
    public static final String TWO = "two";
    public static final String THREE = "three";
}
```

在上述示例中，通过 `reverse` 属性声明了键值对的反转，因此基于该常量类构建的容器可以通过属性值获取属性名。例如，输入 `one`，则可以获得对应的数据源对象为属性名 `ONE`。

:::tip

在 springboot 中，我们也可以在配置文件中直接扫描路径下的所有常量类，然后将其注册为容器。

:::

## 2.3.3. 批量扫描

**通过配置文件**

你可以通过在配置文件在配置文件中配置枚举包路径，以便批量的将扫描到的常量类注册为常量容器：

~~~yml
crane4j:
 # 扫描常量包路径
 container-constant-packages: cn.demo.constant
~~~

**通过注解配置**

在 `2.1.0` 及以上版本，你可以在 spring 的启动类或配置类上通过注解配置要扫描的常量包路径：

~~~java
@EnableCrane4j(
    constantPackages = "com.example.demo", // 要扫描的常量包路径
)
@SpringBootApplication
public class Demo3Application implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(Demo3Application.class, args);
    }
}
~~~

或者，如果不想用 `@EnableCrane4j`，你也可以单独引入某个自动扫描配置，这种方式支持更细粒度的配置：

~~~java
@ContainerConstantScan(
    includePackages = "com.example.demo",  // 要扫描的包路径
    excludeClasses = { NoScanConstant.class }, // 排除指定的类
    includeClasses = { NeedScanConstant.class } // 包含指定的类
)
@SpringBootApplication
public class Demo3Application implements ApplicationRunner {
    public static void main(String[] args) {
        SpringApplication.run(Demo3Application.class, args);
    }
}
~~~