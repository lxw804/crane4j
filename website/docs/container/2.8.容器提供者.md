## 概述

容器提供者 `ContainerProvider` 是用于获取数据源容器的组件，类似于 Spring 中的 `FactoryBean`。通过容器提供者，我们可以更加灵活的获取容器对象。

它一般用于接入第基于三方框架提供的容器机制，比如 `MybatisPlusQueryContainerProvider`，它可以根据指定的配置通过 `BaseMapper` 生成查询容器。

![image-20230311184930927](https://img.xiajibagao.top/image-20230311184930927.png)

## 2.8.1.注册

要注册容器提供者，您可以实现 `ContainerProvider` 接口，并将其声明为 Spring 上下文中的 bean。这样，提供者将自动注册到全局配置中：

~~~java
SimpleCrane4jGlobalConfiguration configuration = SimpleCrane4jGlobalConfiguration.create();
configuration.getContainerProviderMap().put("fooContainerProvider", xxxContainerProvider);
~~~

如果您不是在 Spring 环境中，也可以手动将提供者注册到 `Crane4jGlobalConfiguration` 对象中。

## 2.8.2.使用

用户可通过 `@Assemble` 注解的 `containerProvider` 属性指定该装配操作的容器从哪个提供者获取，比如：

~~~java
public class UserVO {
    @Assemble(
        container = "user", containerProvider = "fooContainerProvider",
        props = @Mapping(src = "name", ref = "name")
    )
    private Integer id;
    private String name;
}
~~~

当配置解析时，`crane4j` 将从用户指定的 `fooContainerProvider` 获取 `namespace` 为 `user` 的数据源容器。

当然，你也可以像 Spring 从 `FactoryBean` 获取 `bean` 那样，通过 `$$` 连接符拼接两者，然后将其作为 `namesapce`，比如上述配置可以改写为：

~~~java
public class UserVO {
    @Assemble(
        container = "fooContainerProvider&&user",
        props = @Mapping(src = "name", ref = "name")
    )
    private Integer id;
    private String name;
}
~~~

